import pandas as pd

from s2m.dataset.coordinates.abstract_coordinates import AbstractCoordinates
from s2m.dataset.observations.observations import AbstractSpatioTemporalObservations


class AnnualMaxima(AbstractSpatioTemporalObservations):

    @classmethod
    def from_coordinates(cls, coordinates: AbstractCoordinates, coordinate_values_to_maxima):
        index_to_maxima = {}
        for i, coordinate_values in coordinates.df_all_coordinates.iterrows():
            if len(coordinate_values) == 1:
                coordinate_values = coordinate_values[0]
            else:
                coordinate_values = tuple([int(v) for v in coordinate_values])
            index_to_maxima[i] = coordinate_values_to_maxima[coordinate_values]
        df = pd.DataFrame(index_to_maxima).transpose()
        df.index = coordinates.index
        return cls(df_maxima_gev=df)








