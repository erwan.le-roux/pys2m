from utils import get_display_name_from_object_type
from s2m.crocus.crocus import CrocusSweTotal, CrocusDepth, CrocusSwe3Days, CrocusSnowLoadTotal, \
    CrocusSnowLoad3Days, CrocusSnowLoad5Days, CrocusSnowLoad7Days
from s2m.crocus.crocus_snow_density import CrocusSnowDensity
from s2m.crocus.crocus_variables import CrocusDensityVariable
from s2m.safran.safran import SafranSnowfall, SafranSnowfall1Day, SafranSnowfall3Days, SafranSnowfall5Days, \
    SafranSnowfall7Days, \
    SafranPrecipitation1Day, SafranTemperature, SafranPrecipitation3Days, SafranPrecipitation5Days, \
    SafranPrecipitation7Days, SafranDateFirstSnowfall

snow_density_str = '$\\rho_{SNOW}$'
eurocode_snow_density = '{}=150 {}'.format(snow_density_str, CrocusDensityVariable.UNIT)
SLEurocode = 'SL from max HS with ' + eurocode_snow_density

SCM_STUDIES = [SafranSnowfall, CrocusSweTotal, CrocusDepth, CrocusSwe3Days]
SCM_STUDIES_NAMES = [get_display_name_from_object_type(k) for k in SCM_STUDIES]
SCM_STUDY_NAME_TO_SCM_STUDY = dict(zip(SCM_STUDIES_NAMES, SCM_STUDIES))




SCM_STUDY_CLASS_TO_ABBREVIATION = {
    SafranSnowfall: 'SF3',
    SafranSnowfall1Day: 'daily snowfall',
    SafranSnowfall3Days: 'SF3',
    SafranSnowfall5Days: 'SF5',
    SafranSnowfall7Days: 'SF7',
    SafranPrecipitation1Day: 'Precipitation',
    SafranTemperature: 'Temperature',
    SafranPrecipitation3Days: 'PR3',
    SafranPrecipitation5Days: 'PR5',
    SafranPrecipitation7Days: 'PR7',
    CrocusSweTotal: 'SWE',
    CrocusSwe3Days: 'SWE3',
    CrocusDepth: 'SD',
    CrocusSnowLoadTotal: 'GSL',
    CrocusSnowLoad3Days: 'GSL3',
    CrocusSnowLoad5Days: 'GSL5',
    CrocusSnowLoad7Days: 'GSL7',
    CrocusSnowDensity: 'Density',
    SafranDateFirstSnowfall: 'SF1 first date'
}

SCM_STUDY_NAME_TO_ABBREVIATION = {get_display_name_from_object_type(k): v for k, v in
                                  SCM_STUDY_CLASS_TO_ABBREVIATION.items()}
SCM_COLORS = ['tab:orange', 'y', 'tab:purple', 'lightseagreen']
SCM_STUDY_CLASS_TO_COLOR = dict(zip(SCM_STUDIES, SCM_COLORS))
SCM_STUDY_NAME_TO_COLOR = {get_display_name_from_object_type(s): color
                           for s, color in zip(SCM_STUDIES, SCM_COLORS)}
poster_altitude_to_color = dict(zip([900, 1800, 2700], ['y', 'tab:purple', 'tab:orange']))

ALL_ALTITUDES = [0, 300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700, 3000, 3300, 3600, 3900, 4200, 4500, 4800]
ALTITUDES_LOW_MIDDLE_HIGH = [900, 1800, 2700]
ALL_ALTITUDES_WITHOUT_NAN = [300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700, 3000, 3300, 3600, 3900, 4200, 4500,
                             4800]
full_altitude_with_at_least_2_stations = [0, 300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700, 3000, 3300, 3600, 3900,
                                          4200]
ALL_ALTITUDES_WITH_20_STATIONS_AT_LEAST = ALL_ALTITUDES[3:-6][:]
