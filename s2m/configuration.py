import os.path as op

from utils import get_root_path

DATA_PATH = op.join(get_root_path(), "data")
MAP_PATH = op.join(DATA_PATH, 'map')
ALPS_FLAT_FOLDER = 'alps_flat'
PYRENEES_FLAT_FOLDER = ''
ALPS_ALLSLOPES_FOLDER = 'alps_allslopes'
