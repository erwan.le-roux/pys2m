from s2m.configuration import MAP_PATH
from utils import cached_property

from matplotlib.lines import Line2D
import io
import os.path as op
from contextlib import redirect_stdout
from typing import Dict, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
from PIL import ImageDraw
from matplotlib.colors import Normalize
from matplotlib.patches import Polygon

from s2m.dataset.coordinates.abstract_coordinates import AbstractCoordinates
from s2m.dataset.coordinates.spatial_coordinates.abstract_spatial_coordinates import AbstractSpatialCoordinates
from s2m.visualization.create_shifted_cmap import get_colors, create_colorbase_axis, get_shifted_map
from s2m.visualization.plot_utils import get_km_formatter

f = io.StringIO()
with redirect_stdout(f):
    pass

class Visualizer(object):

    def __init__(self, study):
        self.study = study

    """ Visualization methods """

    def massifs_coordinates_for_display(self, massif_names) -> AbstractSpatialCoordinates:
        # Coordinate object that represents the massif coordinates in Lambert extended
        # extracted for a csv file, and used only for display purposes
        df = self.load_df_centroid()
        # Lower a bit the Mercantour massif
        df.loc['Mercantour', 'coord_x'] += 14000  # shift to the right
        df.loc['Mercantour', 'coord_y'] -= 7000  # shift down
        # Lower a bit the Maurienne massif
        # df.loc['Mercantour', 'coord_x'] += 14000 # shift to the right
        df.loc['Maurienne', 'coord_y'] -= 6000  # shift down
        df.loc['Maurienne', 'coord_y'] -= 5000  # shift down
        df.loc['Maurienne', 'coord_x'] += 3000  # shift down
        df.loc['Vanoise', 'coord_y'] -= 4000  # shift down
        df.loc['Ubaye', 'coord_y'] -= 4000  # shift down
        # Filter, keep massifs present at the altitude of interest
        df = df.loc[massif_names, :]

        # Build coordinate object from df_centroid
        return AbstractSpatialCoordinates.from_df(df)

    def visualize_study(self, ax=None, massif_name_to_value: Union[None, Dict[str, float]] = None,
                        show=True, fill=True,
                        replace_blue_by_white=False,
                        label=None, add_text=False, cmap=None, add_colorbar=False, vmax=100, vmin=0,
                        default_color_for_missing_massif='gainsboro',
                        default_color_for_nan_values='w',
                        massif_name_to_color=None,
                        show_label=True,
                        scaled=True,
                        fontsize=7,
                        axis_off=False,
                        massif_name_to_hatch_boolean_list=None,
                        norm=None,
                        massif_name_to_marker_style=None,
                        marker_style_to_label_name=None,
                        ticks_values_and_labels=None,
                        massif_name_to_text=None,
                        fontsize_label=15,
                        add_legend=True,
                        ):
        if ax is None:
            ax = plt.gca()

        if massif_name_to_value is not None:
            massif_names, values = list(zip(*massif_name_to_value.items()))
        else:
            massif_names, values = None, None

        if massif_name_to_color is None:
            # Load the colors
            if cmap is None:
                cmap = get_shifted_map(vmin, vmax)
            norm = Normalize(vmin, vmax)
            colors = get_colors(values, cmap, vmin, vmax, replace_blue_by_white)
            massif_name_to_color = dict(zip(massif_names, colors))
        massif_name_to_fill_kwargs = {massif_name: {'color': color} for massif_name, color in
                                      massif_name_to_color.items()}
        massif_names = list(massif_name_to_fill_kwargs.keys())
        masssif_coordinate_for_display = self.massifs_coordinates_for_display(massif_names)

        for coordinate_id, coords_list in self.idx_to_coords_list.items():
            # Retrieve the list of coords (x,y) that define the contour of the massif of id coordinate_id
            # Plot the contour of the massif
            coords_list = list(zip(*coords_list))
            ax.plot(*coords_list, color='black')

            # Potentially, fill the inside of the polygon with some color
            if fill and coordinate_id in self.coordinate_id_to_massif_name:
                massif_name = self.coordinate_id_to_massif_name[coordinate_id]
                if massif_name_to_marker_style is not None and massif_name in massif_name_to_marker_style:
                    massif_coordinate = masssif_coordinate_for_display.df_all_coordinates.loc[massif_name, :].values
                    ax.plot(massif_coordinate[0],
                            massif_coordinate[1], **massif_name_to_marker_style[massif_name])

                if massif_name_to_fill_kwargs is not None and massif_name in massif_name_to_fill_kwargs:
                    fill_kwargs = massif_name_to_fill_kwargs[massif_name]
                    ax.fill(*coords_list, **fill_kwargs)
                else:
                    ax.fill(*coords_list, **{'color': default_color_for_missing_massif})

                # For the moment we comment all the part of this code
                # Add a hatch to visualize the mean & variance variation sign
                hatch_list = ['//', '\\\\']
                if massif_name_to_hatch_boolean_list is not None:
                    if massif_name in massif_name_to_hatch_boolean_list:
                        a = np.array(coords_list).transpose()
                        hatch_boolean_list = massif_name_to_hatch_boolean_list[massif_name]
                        for hatch, is_hatch in zip(hatch_list, hatch_boolean_list):
                            if is_hatch:
                                ax.add_patch(Polygon(xy=a, fill=False, hatch=hatch))

        if show_label:
            # Improve some explanation on the X axis and on the Y axis
            ax.set_xlabel('Longitude (km)')
            ax.xaxis.set_major_formatter(get_km_formatter())
            ax.set_ylabel('Latitude (km)')
            ax.yaxis.set_major_formatter(get_km_formatter())
        else:
            # Remove the ticks
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

        # Display the name or value of the massif
        if add_text:
            for _, row in masssif_coordinate_for_display.df_all_coordinates.iterrows():
                x, y = list(row)
                massif_name = row.name
                if massif_name_to_text is None:
                    value = massif_name_to_value[massif_name]
                    str_value = str(value)
                else:
                    str_value = massif_name_to_text[massif_name]
                ax.text(x, y, str_value, horizontalalignment='center', verticalalignment='center', fontsize=fontsize)

        if scaled:
            plt.axis('scaled')

        # create the colorbar only at the end
        if add_colorbar:
            if len(set(values)) > 1:
                create_colorbase_axis(ax, label, cmap, norm, ticks_values_and_labels=ticks_values_and_labels,
                                      fontsize=fontsize_label)
        if axis_off:
            plt.axis('off')

        # Add legend for the marker
        if add_legend and massif_name_to_marker_style is not None:
            legend_elements = self.get_legend_for_model_symbol(marker_style_to_label_name, markersize=7)
            ax.legend(handles=legend_elements[:], loc='upper right', prop={'size': 9})
            # ax.legend(handles=legend_elements[4:], bbox_to_anchor=(0.01, 0.03), loc='lower left')
            # ax.annotate(' '.join(filled_marker_legend_list),
            #             xy=(0.05, 0.015), xycoords='axes fraction', fontsize=7)

        plt.show()

        return ax

    def get_legend_for_model_symbol(self, marker_style_to_label_name, markersize):
        legend_elements = [
            Line2D([0], [0], marker=marker, color='w', label='${}$'.format(label),
                   markerfacecolor='w', markeredgecolor='k', markersize=markersize)
            for marker, label in marker_style_to_label_name.items()
        ]
        return legend_elements

    @property
    def all_coords_list(self):
        all_values = []
        for e in self.idx_to_coords_list.values():
            all_values.extend(e)
        return list(zip(*all_values))

    @property
    def visualization_x_limits(self):
        return min(self.all_coords_list[0]), max(self.all_coords_list[0])

    @property
    def visualization_y_limits(self):
        return min(self.all_coords_list[1]), max(self.all_coords_list[1])

    @cached_property
    def mask_french_alps(self):
        resolution = 100
        mask_french_alps = np.zeros([resolution, resolution])
        for polygon in self.idx_to_coords_list.values():
            xy_values = list(zip(*polygon))
            normalized_polygon = []
            for values, (minlim, max_lim) in zip(xy_values, [self.visualization_x_limits, self.visualization_y_limits]):
                values -= minlim
                values *= resolution / (max_lim - minlim)
                normalized_polygon.append(values)
            normalized_polygon = list(zip(*normalized_polygon))
            img = Image.new('L', (resolution, resolution), 0)
            ImageDraw.Draw(img).polygon(normalized_polygon, outline=1, fill=1)
            mask_massif = np.array(img)
            mask_french_alps += mask_massif
        return ~np.array(mask_french_alps, dtype=bool)

    def load_df_centroid(self) -> pd.DataFrame:
        # Load df_centroid containing all the massif names
        df_centroid = pd.read_csv(op.join(MAP_PATH, 'coordonnees_massifs_alpes.csv'))
        df_centroid.set_index('NOM', inplace=True)
        # Check that the names of massifs are the same
        symmetric_difference = set(df_centroid.index).symmetric_difference(self.study.all_massif_names)
        assert len(symmetric_difference) == 0, symmetric_difference
        # Sort the column in the order of the SAFRAN dataset
        df_centroid = df_centroid.reindex(self.study.all_massif_names, axis=0)
        for coord_column in [AbstractCoordinates.COORDINATE_X, AbstractCoordinates.COORDINATE_Y]:
            df_centroid.loc[:, coord_column] = df_centroid[coord_column].str.replace(',', '.').astype(float)
        return df_centroid

    @property
    def coordinate_id_to_massif_name(self) -> Dict[int, str]:
        df_centroid = self.load_df_centroid()
        return dict(zip(df_centroid['id'], df_centroid.index))

    @property
    def idx_to_coords_list(self):
        df_massif = pd.read_csv(op.join(MAP_PATH, self.study.csv_file))
        coord_tuples = [(row_massif['idx'], row_massif[AbstractCoordinates.COORDINATE_X],
                         row_massif[AbstractCoordinates.COORDINATE_Y])
                        for _, row_massif in df_massif.iterrows()]
        all_idxs = set([t[0] for t in coord_tuples])
        return {idx: [coords for idx_loop, *coords in coord_tuples if idx == idx_loop] for idx in all_idxs}