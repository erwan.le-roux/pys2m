import os.path as op
import unittest

import numpy as np
import pandas as pd

from s2m.configuration import MAP_PATH
from s2m.crocus.crocus import CrocusSnowLoad3Days
from s2m.safran.safran import SafranSnowfall, SafranPrecipitation, SafranTemperature, SafranSnowfall3Days, \
    SafranRainfall3Days, SafranNormalizedPreciptationRateOnWetDays
from s2m.utils import Season
from s2m.study_utils import SCM_STUDY_CLASS_TO_ABBREVIATION


class TestSCMAllStudy(unittest.TestCase):

    def test_year_to_date(self):
        year = 2019
        study = SafranSnowfall(altitude=900, year_min=year, year_max=year)
        first_day, *_, last_day = study.year_to_days[year]
        self.assertEqual('{}-08-01'.format(year - 1), first_day)
        self.assertEqual('{}-07-31'.format(year), last_day)

    def test_crash(self):
        with self.assertRaises(AssertionError):
            year = 2022
            study = SafranSnowfall(altitude=900, year_min=year, year_max=year)

    def test_year_to_winter_extended_date(self):
        year = 2019
        study = SafranSnowfall(altitude=900, year_min=year, year_max=year, season=Season.winter_extended)
        first_day, *_, last_day = study.year_to_days[year]
        self.assertEqual('{}-11-01'.format(year - 1), first_day)
        self.assertEqual('{}-05-31'.format(year), last_day)
        days = study.year_to_days[year]
        daily_time_series = study.year_to_daily_time_serie_array[year]
        self.assertEqual(len(days), len(daily_time_series))

    def test_study_for_split(self):
        split_years = [1959 + 10 * i for i in range(7)]
        study = SafranSnowfall(altitude=900, split_years=split_years)
        self.assertEqual(split_years, list(study.ordered_years))

    def test_instantiate_studies_with_number_of_days(self):
        altitude = 900
        year_min = 1959
        year_max = 2000
        study_classes = [SafranSnowfall3Days, SafranRainfall3Days, CrocusSnowLoad3Days]
        for study_class in study_classes:
            study_class(altitude=altitude, year_min=year_min, year_max=year_max)

    def test_variables(self):
        for study_class in SCM_STUDY_CLASS_TO_ABBREVIATION.keys():
            study = study_class(year_max=1959)
            _ = study.year_to_annual_maxima[1959]
        self.assertTrue(True)

    # def test_study_visualization(self):
    #     year = 2019
    #     study = SafranSnowfall(altitude=900, year_min=year, year_max=year)
    #
    #     massif_to_value = {m: a[0] for m, a in study.massif_name_to_annual_maxima.items()}
    #     values = list(massif_to_value.values())
    #     vmin, vmax = min(values), max(values)
    #     massif_to_text = {m: round(a) for m, a in massif_to_value.items()}
    #     study.visualize_study(massif_name_to_value=massif_to_value,
    #                           massif_name_to_text=massif_to_text,
    #                           add_text=True,
    #                           add_colorbar=True,
    #                           vmin=vmin, vmax=vmax,
    #                           show=False)
    #     self.assertTrue(True)


class TestSCMSafranNormalizedPrecipitationRateOnWetDays(unittest.TestCase):

    def test_annual_maxima(self):
        study = SafranNormalizedPreciptationRateOnWetDays(year_max=1960)
        self.assertFalse(np.isnan(study.year_to_annual_maxima[1959]).any())


class TestSCMStudy(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.study = None

    def check(self, massif_name_to_value_to_check):
        df_annual_total = self.study.df_annual_total
        for massif_name, value in massif_name_to_value_to_check.items():
            found_value = df_annual_total.loc[:, massif_name].mean()
            self.assertEqual(value, self.round(found_value))

    def round(self, f):
        raise NotImplementedError


class TestSCMSafranSnowfall(TestSCMStudy):

    def setUp(self) -> None:
        super().setUp()
        self.study = SafranSnowfall()

    def test_massif_safran(self):
        df_centroid = pd.read_csv(op.join(MAP_PATH, 'coordonnees_massifs_alpes.csv'))
        # Assert that the massif names are the same between SAFRAN and the coordinate file
        assert not set(self.study.study_massif_names).symmetric_difference(set(df_centroid['NOM']))


if __name__ == '__main__':
    unittest.main()
