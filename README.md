## PyS2M

**1. Clone this git project on your computer**

**2. Install required python packages in a virtualenv**

This project relies on Python3.6, if you do not have it, install it and rely on this python for the following.

You can use the integrated tool of the pycharm IDE to create this virtualenv from the requirements.txt files

or you can use the following command in a terminal located at the root of the project:

$ virtualenv <env_name>

$ source <env_name>/bin/activate

(<env_name>)$ pip install -r requirements.txt

**3. Download files**

Go to https://en.aeris-data.fr/landing-page/?uuid=865730e8-edeb-4c6b-ae58-80f95166509b

Go to the Download section, and select the data to download, e.g. Versions = 2020.2, Areas = Alpes (flat), Products = meteo, Begin year = 1958, End year = 2020

Agree with the data policy, and click on download, rnter your email, then open the received email where you will find a terminal command to download the selected data

Once the data is downloaded, move it in the folder "data" of this project. The first level of this "data" folder corresponds to the "Areas" item, the second level corresponds to the "Products" item, finally all "*.nc" are located at the third level. An example of path: data/alps_flat/pro/PRO_2020080106_2021080106.nc 

Several metadata need to be downloaded. For the French Alps, you can find this metadata in the following google drive folder ( https://drive.google.com/drive/folders/1bZmmYhyvSqlrgAYXnsF_J2hHdgR41ayl?usp=sharing ). Download all the zip files, unzip them, and put them in the "data" folder.

**4. Run the code**

Activate the virtualenv $ source <env_name>/bin/activate

Then, if you download Versions = 2020.2, Areas = Alpes (flat), Products = meteo, Begin year = 1958, End year = 2020

and run the script "examples/example1.py" you should obtain the following figure:

![example1](/uploads/7cef343e16e6e3e1ab7c2fb65ad8e84b/example1.png)

**5. Overview of the main functions**

_Load annual maxima_

    study = SafranTemperature(altitude=2100, year_min=2020, year_max=2020)
    study.massif_name_to_annual_maxima # a dictionary that maps each massif_name to an array of annual maxima
    study.massif_name_to_annual_maxima['Chartreuse'] # gives the annual maxima for the Chartreuse massif
