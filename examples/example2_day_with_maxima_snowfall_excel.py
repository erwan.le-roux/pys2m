import pandas as pd
import numpy as np

from s2m.safran.safran import SafranSnowfall1Day
from s2m.study_utils import SCM_STUDY_CLASS_TO_ABBREVIATION


def generate_excel_with_annual_maxima(fast=True, maxima_dates=False):
    if fast:
        altitudes = [900, 1200]
    else:
        altitudes = [900, 1200, 1500, 1800, 2100]
    study_class = SafranSnowfall1Day
    study_name = 'annual maxima of ' + SCM_STUDY_CLASS_TO_ABBREVIATION[study_class]
    if maxima_dates:
        study_name += ' - number of days since 1st August, e.g. 1 represents the 2nd of August'
    writer = pd.ExcelWriter('{}.xlsx'.format(study_name), engine='xlsxwriter')
    for altitude in altitudes:
        study = study_class(altitude=altitude)
        write_df_with_annual_maxima_v2(altitude, writer, study, maxima_dates)
    writer.save()


def write_df_with_annual_maxima_v2(altitude, writer, study, maxima_dates=False) -> pd.DataFrame:
    df = study.df_latitude_longitude
    data_list = []
    for massif_name in df.index:
        if maxima_dates:
            values = study.massif_name_to_annual_maxima_angle[massif_name]
        else:
            raise NotImplementedError
        data_list.append(values)
    data = np.array(data_list)
    df2 = pd.DataFrame(data=data, index=df.index, columns=study.ordered_years).astype(float)
    df = pd.concat([df, df2], axis=1)
    print(df.head())
    df.to_excel(writer, sheet_name='altitude = {} m'.format(altitude))


if __name__ == '__main__':
    generate_excel_with_annual_maxima(fast=False, maxima_dates=True)
