import matplotlib.pyplot as plt
from s2m.safran.safran import SafranTemperature, SafranPrecipitation
from s2m.visualization.visualizer import Visualizer

altitude = 2100
study = SafranTemperature(altitude=altitude, year_min=2020, year_max=2020)
annual_maxima = study.massif_name_to_annual_maxima["Chartreuse"]

visualizer = Visualizer(study)
massif_name_to_value = {
    m: v[0] for m, v in study.massif_name_to_annual_maxima.items()
}
values = list(massif_name_to_value.values())
massif_to_text = {
 m: str(round(v, 1)) + '$^o$C' for m, v in massif_name_to_value.items()
}
visualizer.visualize_study(massif_name_to_value=massif_name_to_value, cmap=plt.cm.Reds,
                           massif_name_to_text=massif_to_text, add_text=True,
                           vmin=min(values), vmax=max(values),
                           add_colorbar=True, label='Annual maximum of temperature\nat {} m'
                                                    ' between the 1st August 2019\n and'
                                                    ' the 31st of July 2020 ($^o$C)'.format(altitude))