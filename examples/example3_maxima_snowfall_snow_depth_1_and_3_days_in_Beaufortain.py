import pandas as pd

from s2m.abstract_study import AbstractStudy
from s2m.crocus.crocus import CrocusSwe3Days, CrocusDepth
from s2m.safran.safran import SafranSnowfall1Day, SafranSnowfall3Days
from s2m.utils import FrenchRegion


def generate_excel_with_annual_maxima(fit=True):
    massif_name, french_region, altitudes = 'Beaufortain', FrenchRegion.alps, [900, 1200]
    # massif_name, french_region, altitudes = 'haute-bigorre', FrenchRegion.pyrenees, [300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700, 3000, 3300]
    study_classes = [SafranSnowfall1Day, SafranSnowfall3Days, CrocusSwe3Days, CrocusDepth]
    writer = pd.ExcelWriter('{}.xlsx'.format(massif_name), engine='xlsxwriter')
    # writer = pd.ExcelWriter('pandas_multiple.xlsx')
    for j, study_class in enumerate(study_classes, 1):
        write_df_with_annual_maxima(massif_name, french_region, writer, study_class, altitudes, fit=fit)
    writer.save()


def write_df_with_annual_maxima(massif_name, french_region, writer, study_class, altitudes, fit=True) -> pd.DataFrame:
    columns = []
    for altitude in altitudes:
        study = study_class(altitude=altitude, french_region=french_region) # type: AbstractStudy
        df_maxima = study.observations_annual_maxima.df_maxima_gev
        s = df_maxima.loc[massif_name]
        columns.append(s)
    df = pd.concat(columns, axis=1)
    altitude_str = [str(a) + ' m' for a in altitudes]
    df.columns = altitude_str
    name = study.variable_name
    short_name = name[:31]
    df.to_excel(writer, sheet_name=short_name)
    return df


if __name__ == '__main__':
    generate_excel_with_annual_maxima(fit=True)
